#ifndef HARDWARE_MANAGER_H_
#define HARDWARE_MANAGER_H

#include "Arduino.h"

// Define the sensor data struct
// you can change this with more sensors if you want!
typedef struct {
    // IMU Data
    double accel_x;
    double accel_y;
    double accel_z;
    double angular_velocity_x;
    double angular_velocity_y;
    double angular_velocity_z;
    double magnetometer_x;
    double magnetometer_y;
    double magnetometer_z;
    // Barometer Data
    double pressure;
    double temperature;
} SensorData_t;


// Function prototypes


// Initializes all sensors and peripherals (like the SD card)
void HM_Init(SensorData_t &sensorData);

// Get new values from all initialized sensors
// This modifies the struct we pass in (via reference)
// to set the new values
void HM_ReadSensorData(SensorData_t &sensorData);

// Logs the given data packet to the SD card
void HM_LogDataToSD(SensorData_t &sensorData);

// Logs a state transition to the SD card
void HM_LogTransitionToSD(String logString);

#endif // HARDWARE_MANAGER_H_
