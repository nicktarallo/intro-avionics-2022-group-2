#include "hardware_manager.h"
#include "movingAvg.h"

enum states {
  PREFLIGHT,
  ASCENT,
  DESCENT,
  POSTFLIGHT
};

// Creates an instance of the SensorData struct
SensorData_t sensorData;

// Start in PREFLIGHT
int state = PREFLIGHT;

// Create a moving average to store previous 20 pressure values
movingAvg pressure(15);  // changed from 20

void setup() {
  Serial.begin(9600);
  HM_Init(sensorData);
  pressure.begin();
}


bool transitionCondition() {
  return true;
}

uint32_t accelThresholdTrueSince = millis();
uint32_t touchdownTimer = millis();
uint32_t lastIntervalPressure = 2 * 10000;


// max altitude occurs at min pressure
uint32_t minPressure = 2 * 10000; 
uint32_t prevPrint = millis();
#define PRINT_DELAY 250
void loop() {
  HM_ReadSensorData(sensorData);
  // The moving average library only uses ints
  // So we multiply our float by 10000 then cast as int
  pressure.reading((uint32_t)(sensorData.pressure*10000));

  uint32_t newPres = pressure.getAvg();
  if (state > PREFLIGHT) {
    if (newPres < minPressure) {
      minPressure = newPres;
    }
  }

   if (millis() - prevPrint > PRINT_DELAY) {
    prevPrint = millis();
    //Serial.print("State: ");
    Serial.print(6000 + (state * 1000));
    Serial.print(" ");
    //Serial.print("NewPres: ");
    Serial.print(newPres);
    //Serial.print("Check: ");
   // Serial.println(altitudeCheck);
    //Serial.print("Data: ");
    Serial.print(" ");
    Serial.print(sensorData.pressure*10000);
    Serial.print(" ");
    //Serial.print("Accz: ");
    //Serial.print(" ");
    Serial.println(6000 + (sensorData.accel_z * 100));
  }
  switch(state) {
    case PREFLIGHT:
      // If our accel isn't what we want, reset the timer
      if (!(sensorData.accel_z > 20)) {
        accelThresholdTrueSince = millis();
      }
      // If we get here, our accel has been at the threshold for 300ms
      // So we should transition!
      if (millis() - accelThresholdTrueSince > 300) {
        state = ASCENT;
      }
      // Code for PREFLIGHT
      break;
    case ASCENT:
      // Higher pressure = Going down
      // This means we are past apogee - go to descent
      if (newPres > (minPressure + 10)) {  // changed from 10
        state = DESCENT;
        lastIntervalPressure = newPres;
        touchdownTimer = millis();
      }
      // Code for ASCENT
      break;
    case DESCENT:
      // TODO: Implement descent transition to postflight
      if (newPres - lastIntervalPressure > 10) {  // changed from 10
        lastIntervalPressure = newPres;
        touchdownTimer = millis();
      }
      if (millis() - touchdownTimer > 3000) {  // changed from 3000
        state = POSTFLIGHT;
      }
      // Code for DESCENT
      break;
    case POSTFLIGHT:
      // We can't leave POSTFLIGHT so no transition code needed here
      // Code for POSTFLIGHT
      break;
    default:
      // Shouldn't get here
      break;
  }
  // Delay for a bit
  delay(10);
}
